// Basic Gulp File
//
var gulp = require('gulp'),
   sass = require('gulp-sass'),
   autoprefix = require('gulp-autoprefixer'),
   notify = require("gulp-notify"),
   bower = require('gulp-bower'),
   webserver = require('gulp-webserver'),
   minify = require('gulp-minifier'),
   rename = require("gulp-rename"),
   concat = require('gulp-concat'),
   image = require('gulp-image');

var config = {
   sassPath: './public/stylesheets/scss',
   bowerDir: './bower_components'
}

gulp.task('bower', function() {
   return bower()
       .pipe(gulp.dest(config.bowerDir))
})

gulp.task('css', function() {
      gulp.src([config.sassPath + '/*.scss', config.bowerDir + '/bootstrap/dist/css/bootstrap.min.css'])

   .pipe(sass().on('error', sass.logError))
   .pipe(autoprefix('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
  .pipe(minify({
     minifyCSS: true
   }))
   .pipe(concat('style.css'))
   .pipe(rename({suffix: '.min'}))
   .pipe(gulp.dest('public/stylesheets/css'));
})

gulp.task("optimize-js", function () {
  return gulp.src([
      config.bowerDir + '/jquery/dist/jquery.min.js',
      config.bowerDir + '/bootstrap/dist/js/bootstrap.min.js'
    
    ])
    .pipe(concat('bundle.js'))
    .pipe(gulp.dest('./public/js/'));
});


//image-min
gulp.task('images', function () {
    return gulp.src(['./public/images/*.png', '!/public/images/*.db'])
        .pipe(image())
        .pipe(gulp.dest('./public/images/min'));
});


gulp.task('webserver', function() {
  gulp.src('./')
    .pipe(webserver({
      livereload: true,
      directoryListing: true,
      open: true
    }));
});

// Rerun the task when a file changes
gulp.task('watch', function() {
   gulp.watch(config.sassPath + '/*.scss', ['css']);
});

gulp.task('default', ['webserver','bower', 'images', 'css']);
